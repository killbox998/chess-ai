use crate::{chess::*, game::PlayerLogic};

use rayon::prelude::*;

//use rand::{thread_rng, Rng};

pub struct AI;

impl AI {
    pub fn new() -> Self {
        AI {}
    }
}

impl PlayerLogic for AI {
    fn get_move(&mut self, board: Board, side: Side) -> Result<Move, ChessError> {
        let mut map = ChessMap::new(board, side);
        Ok(map.best_move().m)
    }
}

fn eval_board(board: Board, side: Side) -> i32 {
    let mut sum: i32 = 0;
    for (piece, _) in board {
        if let Some(piece) = piece {
            if piece.side == side {
                sum += piece_weights(piece.piece);
            } else {
                sum -= piece_weights(piece.piece);
            }
        }
    }
    sum += crate::chess::all_valid_moves(board, side).unwrap().len() as i32;
    sum
}

const fn piece_weights(chess: ChessPiece) -> i32 {
    match chess {
        ChessPiece::King => 1000,
        ChessPiece::Queen => 50,
        ChessPiece::Knight => 10,
        ChessPiece::Rook => 15,
        ChessPiece::Bishop => 15,
        ChessPiece::Pawn => 5,
    }
}

const MAX_DEPTH: u32 = 3;

struct ChessMap {
    root: Board,
    side: Side,
    sub_moves: Option<Vec<ChessMapNode>>,
}
impl ChessMap {
    pub fn new(board: Board, side: Side) -> Self {
        Self {
            root: board,
            side,
            sub_moves: None,
        }
    }

    pub fn replace_root(&mut self, n: ChessMapNode) {
        self.root = n.m.execute().unwrap();
        self.sub_moves = n.sub_moves
    }

    fn best_move(&mut self) -> ChessMapNode {
        println!("Finding Best move");
        if self.sub_moves.is_none() {
            self.generate_moves();
        }
        let move_set = std::mem::replace(&mut self.sub_moves, None);
        move_set
            .unwrap()
            .into_par_iter()
            .max_by_key(|x| x.eval_map())
            .unwrap()
    }

    fn generate_moves(&mut self) {
        println!("Generating moves");
        if self.sub_moves.is_none() {
            let board = self.root;
            let mut moves: Vec<_> = all_valid_moves(board, self.side)
                .unwrap()
                .into_iter()
                .map(|x| ChessMapNode::new(x))
                .collect();
            self.sub_moves = Some(moves);
        }
        self.sub_moves
            .as_mut()
            .unwrap()
            .par_iter_mut()
            .for_each(|x| x.generate_moves(1));

        println!("Generation done");
    }
}

struct ChessMapNode {
    m: Move,
    sub_moves: Option<Vec<ChessMapNode>>,
}

impl ChessMapNode {
    pub fn new(m: Move) -> Self {
        ChessMapNode { m, sub_moves: None }
    }

    fn eval_map(&self) -> i32 {
        if let Some(move_list) = &self.sub_moves {
            move_list.iter().map(|x| x.eval_map()).max().unwrap()
        } else {
            eval_board(self.m.get_board(), self.m.get_piece().unwrap().side)
        }
    }

    fn generate_moves(&mut self, depth: u32) {
        if depth >= MAX_DEPTH {
            return;
        }
        if self.sub_moves.is_none() {
            self.m.validate().unwrap();
            let side = self
                .m
                .get_board()
                .get_piece(self.m.get_start_index())
                .unwrap()
                .side;
            let board = self.m.execute().unwrap();
            let mut moves: Vec<_> = all_valid_moves(board, side.invert())
                .unwrap()
                .into_iter()
                .map(|x| ChessMapNode::new(x))
                .collect();
            self.sub_moves = Some(moves);
        }
        for cmn in self.sub_moves.as_mut().unwrap().iter_mut() {
            cmn.generate_moves(depth + 1);
        }
    }
}
