use crate::chess::*;

use anyhow::Result;
use std::io;
use std::io::Write;
pub struct Game {
    board: Board,
    current_move: Side,
}

impl Game {
    pub fn new() -> Self {
        Game {
            board: Board::new(),
            current_move: Side::White,
        }
    }

    pub fn get_board(&self) -> &Board {
        &self.board
    }

    pub fn run_single_move<F>(&mut self, mut given_move: F) -> Result<(), ChessError>
    where
        F: FnMut(Board, Side) -> Result<Move, ChessError>,
    {
        let new_move = given_move(self.board, self.current_move)?;
        if let Some(piece) = new_move.get_piece() {
            if piece.side != self.current_move {
                Err(MoveCreationError::Aborted)?;
            }
        }

        self.board = new_move.execute()?;
        self.current_move = self.current_move.invert();

        Ok(())
    }
}
pub trait PlayerLogic {
    fn get_move(&mut self, board: Board, side: Side) -> Result<Move, ChessError>;
}

pub struct Player {
    side: Side,
    logic: Box<dyn PlayerLogic>,
}

impl Player {
    fn new(side: Side, logic: Box<dyn PlayerLogic>) -> Self {
        Player { logic, side }
    }
    fn get_move(&mut self, board: Board) -> Result<Move, ChessError> {
        self.logic.get_move(board, self.side)
    }
}
