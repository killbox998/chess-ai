#![feature(generators)]
#![feature(conservative_impl_trait)]
#![feature(generator_trait)]

mod ai;
mod chess;
mod game;
mod render;

use std::{thread::sleep, time::Duration, time::Instant};

use ai::AI;
use anyhow::{Context, Result};
use chess::{ChessError, MoveCreationError};
use game::*;
use render::{RenderingContext, Textures};
use sdl2::{event::Event, EventPump};

const FPS: u64 = 60;
const MICROS_PER_FRAME: u64 = 1000000 / FPS;

fn main() -> Result<()> {
    let mut render =
        RenderingContext::new(1920, 1080, "Chess").context("Failed to create rendering context")?;
    let creator = render.get_texture_creator();
    let textures = Textures::new(&creator);

    let mut event_pump: EventPump = render.get_event_pump();
    let mut game = Game::new();
    let mut ai = AI::new();
    'main: loop {
        let start = Instant::now();
        render.draw_board(game.get_board(), &textures);

        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. } => break 'main,
                Event::MouseButtonDown { x, y, .. } => {
                    process_click(x, y, &mut render, &mut game, &mut ai);
                }

                _ => {}
            }
        }
        let frame_time = start.elapsed().as_micros();
        if frame_time < MICROS_PER_FRAME as u128 {
            sleep(Duration::from_micros(MICROS_PER_FRAME - frame_time as u64))
        }
    }
    Ok(())
}

fn process_click(x: i32, y: i32, render: &mut RenderingContext, game: &mut Game, ai: &mut AI) {
    //was the click on the board?
    if let Ok((x, y)) = get_board_index(x as u32, y as u32, render.square_size) {
        match game.run_single_move(|board, side| {
            //is there already a square selected?
            if let Some(sel) = render.selected_square {
                if render
                    .potential_moves
                    .as_ref()
                    .unwrap()
                    .iter()
                    .any(|m| m.get_end_index() == (x as usize, y as usize))
                {
                    let m = render
                        .potential_moves
                        .take()
                        .unwrap()
                        .into_iter()
                        .find(|m| m.get_end_index() == (x as usize, y as usize))
                        .unwrap();

                    render.selected_square = None;
                    render.potential_moves = None;
                    return Ok(m);
                } else if (x as usize, y as usize) == sel {
                    render.selected_square = None;
                    render.potential_moves = None;
                    return Err(ChessError::MoveCreation{creation_error: MoveCreationError::NotComplete});
                }
                //select square
            } else {
                if let Some(piece) = board.get_piece((x as usize, y as usize)) {
                    //abort if trying to select the opponents piece
                    if piece.side == side {
                        render.selected_square = Some((x as usize, y as usize));
                        render.potential_moves =
                            Some(chess::chess_move_generation::get_valid_moves(
                                board,
                                render.selected_square.unwrap(),
                            ));
                            return Err(ChessError::MoveCreation{creation_error: MoveCreationError::NotComplete});
                    }
                }
            }

            Err(ChessError::MoveCreation{creation_error: MoveCreationError::Aborted})
        }) {
            Ok(_) => game
                .run_single_move(|board, side| ai.get_move(board, side))
                .unwrap(),
            Err(e) => {
                
            }
        }
    }
}

fn get_board_index(
    mut x: u32,
    mut y: u32,
    square_size: u32,
) -> Result<(u32, u32), chess::ChessError> {
    let mut x_offset = 0;
    let mut y_offset = 0;

    while x > square_size {
        x_offset += 1;
        x -= square_size;
    }
    while y > square_size {
        y_offset += 1;
        y -= square_size;
    }
    if x_offset < 8 && y_offset < 8 {
        Ok((x_offset, y_offset))
    } else {
        Err(chess::ChessError::InvalidIndex)
    }
}
