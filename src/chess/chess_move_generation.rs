use super::{is_in_check, Board, Move, SpecialMove};

///index must point to a pawn
fn get_valid_pawn_moves(board: Board, index: (usize, usize)) -> Vec<Move> {
    let mut moves: Vec<Move> = Vec::new();
    let piece = board.get_piece(index).unwrap();
    match piece.side {
        super::Side::White => {
            if index.1 == 1 {
                if board.get_piece((index.0, index.1 + 2)).is_none()
                    && board.get_piece((index.0, index.1 + 1)).is_none()
                {
                    let mut m = Move::new(board, index, (index.0, index.1 + 2), None).unwrap();
                    if !is_in_check(m.execute_without_validation().unwrap(), piece.side) {
                        unsafe { m.force_validate() }
                        moves.push(m);
                    }
                }
            }
            if index.1 < 7 {
                if board.get_piece((index.0, index.1 + 1)).is_none() {
                    let mut m = Move::new(board, index, (index.0, index.1 + 1), None).unwrap();
                    if !is_in_check(m.execute_without_validation().unwrap(), piece.side) {
                        unsafe { m.force_validate() }
                        moves.push(m);
                    }
                }

                if index.0 > 0 {
                    if let Some(cap) = board.get_piece((index.0 - 1, index.1 + 1)) {
                        if cap.side != piece.side {
                            let mut m =
                                Move::new(board, index, (index.0 - 1, index.1 + 1), None).unwrap();
                            if !is_in_check(m.execute_without_validation().unwrap(), piece.side) {
                                unsafe { m.force_validate() }
                                moves.push(m);
                            }
                        }
                    }
                }
                if index.0 < 7 {
                    if let Some(cap) = board.get_piece((index.0 + 1, index.1 + 1)) {
                        if cap.side != piece.side {
                            let mut m =
                                Move::new(board, index, (index.0 + 1, index.1 + 1), None).unwrap();
                            if !is_in_check(m.execute_without_validation().unwrap(), piece.side) {
                                unsafe { m.force_validate() }
                                moves.push(m);
                            }
                        }
                    }
                }
            }
        }
        super::Side::Black => {
            if index.1 == 6 {
                if board.get_piece((index.0, index.1 - 2)).is_none()
                    && board.get_piece((index.0, index.1 - 1)).is_none()
                {
                    let mut m = Move::new(board, index, (index.0, index.1 - 2), None).unwrap();
                    if !is_in_check(m.execute_without_validation().unwrap(), piece.side) {
                        unsafe { m.force_validate() }
                        moves.push(m);
                    }
                }
            }
            if index.1 > 0 {
                if board.get_piece((index.0, index.1 - 1)).is_none() {
                    let mut m = Move::new(board, index, (index.0, index.1 - 1), None).unwrap();
                    if !is_in_check(m.execute_without_validation().unwrap(), piece.side) {
                        unsafe { m.force_validate() }
                        moves.push(m);
                    }
                }

                if index.0 > 0 {
                    if let Some(cap) = board.get_piece((index.0 - 1, index.1 - 1)) {
                        if cap.side != piece.side {
                            let mut m =
                                Move::new(board, index, (index.0 - 1, index.1 - 1), None).unwrap();
                            if !is_in_check(m.execute_without_validation().unwrap(), piece.side) {
                                unsafe { m.force_validate() }
                                moves.push(m);
                            }
                        }
                    }
                }
                if index.0 < 7 {
                    if let Some(cap) = board.get_piece((index.0 + 1, index.1 - 1)) {
                        if cap.side != piece.side {
                            let mut m =
                                Move::new(board, index, (index.0 + 1, index.1 - 1), None).unwrap();
                            if !is_in_check(m.execute_without_validation().unwrap(), piece.side) {
                                unsafe { m.force_validate() }
                                moves.push(m);
                            }
                        }
                    }
                }
            }
        }
    }
    moves
}
///index must point to a king
fn get_valid_king_moves(board: Board, index: (usize, usize)) -> Vec<Move> {
    let mut moves: Vec<Move> = Vec::new();
    for y in -1..=1 {
        for x in -1..=1 {
            if !(y == 0 && x == 0)
                && index.0 as i32 + x >= 0
                && index.0 as i32 + x <= 7
                && index.1 as i32 + y >= 0
                && index.1 as i32 + y <= 7
            {
                if let Some(piece) =
                    board.get_piece(((index.0 as i32 + x) as usize, (index.1 as i32 + y) as usize))
                {
                    if piece.side != board.get_piece(index).unwrap().side {
                        let mut m = Move::new(
                            board,
                            index,
                            ((index.0 as i32 + x) as usize, (index.1 as i32 + y) as usize),
                            None,
                        )
                        .unwrap();
                        if !is_in_check(
                            m.execute_without_validation().unwrap(),
                            board.get_piece(index).unwrap().side,
                        ) {
                            m.validate().unwrap();
                            moves.push(m);
                        }
                    }
                } else {
                    let mut m = Move::new(
                        board,
                        index,
                        ((index.0 as i32 + x) as usize, (index.1 as i32 + y) as usize),
                        None,
                    )
                    .unwrap();
                    if !is_in_check(
                        m.execute_without_validation().unwrap(),
                        board.get_piece(index).unwrap().side,
                    ) {
                        m.validate().unwrap();
                        moves.push(m);
                    }
                }
            }
        }
    }

    let piece = board.get_piece(index).unwrap();
    if !board.get_player_info(piece.side).has_king_moved && !is_in_check(board, piece.side) {
        if !board.get_player_info(piece.side).has_king_rook_moved {
            match piece.side {
                super::Side::White => {
                    if board.get_piece((1, 0)).is_none()
                        && board.get_piece((2, 0)).is_none()
                        && !is_in_check(board, piece.side)
                    {
                        let mut m =
                            Move::new(board, index, (1, 0), Some(SpecialMove::Castle)).unwrap();
                        if !is_in_check(m.execute_without_validation().unwrap(), piece.side) {
                            unsafe {
                                m.force_validate();
                            }
                            moves.push(m);
                        }
                    }
                }
                super::Side::Black => {
                    if board.get_piece((1, 7)).is_none() && board.get_piece((2, 7)).is_none() {
                        let mut m =
                            Move::new(board, index, (1, 0), Some(SpecialMove::Castle)).unwrap();
                        if !is_in_check(m.execute_without_validation().unwrap(), piece.side) {
                            unsafe {
                                m.force_validate();
                            }
                            moves.push(m);
                        }
                    }
                }
            }
        }
        if !board.get_player_info(piece.side).has_queen_rook_moved {
            match piece.side {
                super::Side::White => {
                    if board.get_piece((5, 0)).is_none()
                        && board.get_piece((4, 0)).is_none()
                        && board.get_piece((6, 0)).is_none()
                    {
                        let mut m =
                            Move::new(board, index, (5, 0), Some(SpecialMove::Castle)).unwrap();
                        if !is_in_check(m.execute_without_validation().unwrap(), piece.side) {
                            unsafe {
                                m.force_validate();
                            }
                            moves.push(m);
                        }
                    }
                }
                super::Side::Black => {
                    if board.get_piece((5, 7)).is_none()
                        && board.get_piece((4, 7)).is_none()
                        && board.get_piece((6, 7)).is_none()
                    {
                        let mut m =
                            Move::new(board, index, (5, 7), Some(SpecialMove::Castle)).unwrap();
                        if !is_in_check(m.execute_without_validation().unwrap(), piece.side) {
                            unsafe {
                                m.force_validate();
                            }
                            moves.push(m);
                        }
                    }
                }
            }
        }
    }

    moves
}
///index must point to a rook
fn get_valid_rook_moves(board: Board, index: (usize, usize)) -> Vec<Move> {
    todo!()
}
///index must point to a bishop
fn get_valid_bishop_moves(board: Board, index: (usize, usize)) -> Vec<Move> {
    todo!()
}
///index must point to a queen
fn get_valid_queen_moves(board: Board, index: (usize, usize)) -> Vec<Move> {
    todo!()
}
///index must point to a knight
fn get_valid_knight_moves(board: Board, index: (usize, usize)) -> Vec<Move> {
    todo!()
}

fn get_generic_moves(board: Board, index: (usize, usize)) -> Vec<Move> {
    let mut list = Vec::new();
    for (_, (x, y)) in board.iter() {
        let mut tested_move = Move::new(board, index, (x, y), None).unwrap();
        let _ = tested_move.validate();
        if tested_move.is_valid() {
            list.push(tested_move);
        }
    }
    list
}

pub(crate) fn get_valid_moves(board: Board, index: (usize, usize)) -> Vec<Move> {
    if let Some(piece) = board.get_piece(index) {
        match piece.piece {
            super::ChessPiece::King => get_valid_king_moves(board, index),
            super::ChessPiece::Queen => get_generic_moves(board, index),
            super::ChessPiece::Knight => get_generic_moves(board, index),
            super::ChessPiece::Rook => get_generic_moves(board, index),
            super::ChessPiece::Bishop => get_generic_moves(board, index),
            super::ChessPiece::Pawn => get_valid_pawn_moves(board, index),
        }
    } else {
        Vec::new()
    }
}
