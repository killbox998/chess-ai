use std::fmt::*;

use super::*;

#[derive(Eq, PartialEq, Clone, Copy, Debug)]
pub enum EndGameState {
    Checkmate,
    Stalemate,
}

#[derive(Eq, PartialEq, Clone, Copy, Debug)]
pub struct PlayerBoardInfo {
    pub has_king_moved: bool,
    pub has_king_rook_moved: bool,
    pub has_queen_rook_moved: bool,
    pub end_state: Option<EndGameState>,
}

impl PlayerBoardInfo {
    pub const fn new() -> Self {
        PlayerBoardInfo {
            has_king_moved: false,
            has_king_rook_moved: false,
            has_queen_rook_moved: false,
            end_state: None,
        }
    }
}

#[derive(Eq, PartialEq, Clone, Copy, Debug)]
pub struct Board {
    data: [Option<GamePiece>; 64],
    white: PlayerBoardInfo,
    black: PlayerBoardInfo,
}

impl Board {
    ///Set a position on the board
    /// # saftey
    /// PlayerBoardInfo must be checked to be correct
    pub fn set_piece(&mut self, index: (usize, usize), piece: Option<GamePiece>) {
        self.data[index.0 + index.1 * 8] = piece;
    }
    pub const fn get_piece(&self, index: (usize, usize)) -> &Option<GamePiece> {
        &self.data[index.0 + (index.1 * 8)]
    }

    pub const fn is_game_over(&self) -> bool {
        self.white.end_state.is_some() || self.black.end_state.is_some()
    }

    pub const fn get_player_info(&self, side: Side) -> PlayerBoardInfo {
        match side {
            White => self.white,
            Black => self.black,
        }
    }
    pub fn set_player_info(
        &mut self,
        side: Side,
        king_moved: Option<bool>,
        king_rook_moved: Option<bool>,
        queen_rook_moved: Option<bool>,
    ) {
        let player_info = match side {
            White => &mut self.white,
            Black => &mut self.black,
        };
        if let Some(king_moved) = king_moved {
            player_info.has_king_moved = king_moved;
        }
        if let Some(king_rook_moved) = king_rook_moved {
            player_info.has_king_rook_moved = king_rook_moved;
        }
        if let Some(queen_rook_moved) = queen_rook_moved {
            player_info.has_queen_rook_moved = queen_rook_moved;
        }
    }
    pub fn set_end_state(&mut self, state: Option<EndGameState>, side: Side) {
        match side {
            White => self.white.end_state = state,
            Black => self.black.end_state = state,
        }
    }

    pub fn new() -> Self {
        let mut board = Board {
            data: [None; 64],
            white: PlayerBoardInfo::new(),
            black: PlayerBoardInfo::new(),
        };

        //create pawns
        for i in 0..8 {
            board.set_piece((i, 1), Some(GamePiece::new(Pawn, White)));
            board.set_piece((i, 6), Some(GamePiece::new(Pawn, Black)));
        }
        board.set_piece((0, 0), Some(GamePiece::new(Rook, White)));
        board.set_piece((7, 0), Some(GamePiece::new(Rook, White)));
        board.set_piece((1, 0), Some(GamePiece::new(Knight, White)));
        board.set_piece((6, 0), Some(GamePiece::new(Knight, White)));
        board.set_piece((2, 0), Some(GamePiece::new(Bishop, White)));
        board.set_piece((5, 0), Some(GamePiece::new(Bishop, White)));
        board.set_piece((3, 0), Some(GamePiece::new(King, White)));
        board.set_piece((4, 0), Some(GamePiece::new(Queen, White)));

        board.set_piece((0, 7), Some(GamePiece::new(Rook, Black)));
        board.set_piece((7, 7), Some(GamePiece::new(Rook, Black)));
        board.set_piece((1, 7), Some(GamePiece::new(Knight, Black)));
        board.set_piece((6, 7), Some(GamePiece::new(Knight, Black)));
        board.set_piece((2, 7), Some(GamePiece::new(Bishop, Black)));
        board.set_piece((5, 7), Some(GamePiece::new(Bishop, Black)));
        board.set_piece((3, 7), Some(GamePiece::new(King, Black)));
        board.set_piece((4, 7), Some(GamePiece::new(Queen, Black)));

        board
    }

    pub fn new_custom(pieces: Vec<(GamePiece, usize, usize)>) -> Self {
        let mut board = Board {
            data: [None; 64],
            white: PlayerBoardInfo::new(),
            black: PlayerBoardInfo::new(),
        };
        for a in pieces.iter() {
            board.data[a.1 + a.2 * 8] = Some(a.0);
        }
        board
    }

    pub fn iter(&self) -> impl Iterator<Item = (Option<GamePiece>, (usize, usize))> {
        BoardIter::new(*self)
    }
}

impl IntoIterator for Board {
    type Item = (Option<GamePiece>, (usize, usize));

    type IntoIter = BoardIter;

    fn into_iter(self) -> Self::IntoIter {
        BoardIter::new(self)
    }
}

impl fmt::Display for Board {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut str = String::new();
        str.push_str("  ABCDEFGH  \n");
        str.push_str(" ┌────────┐\n");
        for y in 0..8 {
            str.push_str(&format!("{}│", y + 1));
            for x in 0..8 {
                str.push(match self.get_piece((x, y)) {
                    None => {
                        /*if (x + (y * 8) - y - 1) % 2 == 0 {
                            ' '
                        } else {
                            '▒'
                        }*/
                        ' '
                    }
                    Some(piece) => {
                        let ch = match piece.piece {
                            King => 'k',
                            Queen => 'q',
                            Rook => 'r',
                            Bishop => 'b',
                            Knight => 'n',
                            Pawn => 'p',
                        };
                        match piece.side {
                            White => ch.to_ascii_uppercase(),
                            Black => ch.to_ascii_lowercase(),
                        }
                    }
                });
            }
            str.push('│');
            str.push('\n');
        }
        str.push_str(" └────────┘\n");
        write!(f, "{}", str)
    }
}

pub struct BoardIter {
    board: Board,
    x: usize,
    y: usize,
}
impl BoardIter {
    pub fn new(board: Board) -> Self {
        Self { board, x: 0, y: 0 }
    }
}

impl Iterator for BoardIter {
    type Item = (Option<GamePiece>, (usize, usize));

    fn next(&mut self) -> Option<Self::Item> {
        if self.y > 7 {
            return None;
        }
        let out = (*self.board.get_piece((self.x, self.y)), (self.x, self.y));
        self.x += 1;
        if self.x == 8 {
            self.x = 0;
            self.y += 1;
        }
        Some(out)
    }
}
