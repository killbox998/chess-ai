use super::*;
use std::cmp::Ordering;
use std::error::Error;
use std::fmt;
use std::str::FromStr;
use thiserror::Error;
use Direction::*;

#[derive(Eq, PartialEq, Copy, Clone, Debug)]
enum Direction {
    NoDirection,
    Up,
    Down,
    Left,
    Right,
    UpLeft,
    DownLeft,
    UpRight,
    DownRight,
}

impl Direction {
    ///Returns a direction when given and x and y delta
    ///
    ///Positive is down and right. Negative is up and left
    fn new(x_delta: i32, y_delta: i32) -> Self {
        match x_delta.cmp(&0) {
            Ordering::Less => match y_delta.cmp(&0) {
                Ordering::Less => UpLeft,
                Ordering::Equal => Left,
                Ordering::Greater => DownLeft,
            },
            Ordering::Equal => match y_delta.cmp(&0) {
                Ordering::Less => Up,
                Ordering::Equal => NoDirection,
                Ordering::Greater => Down,
            },
            Ordering::Greater => match y_delta.cmp(&0) {
                Ordering::Less => UpRight,
                Ordering::Equal => Right,
                Ordering::Greater => DownRight,
            },
        }
    }
}

#[derive(Error, Debug, PartialEq, Eq)]
pub enum MoveCreationError {
    #[error("The Move has been aborted for some reason")]
    Aborted,
    #[error("The Move is not complete")]
    NotComplete,
}

///Special moves are any move that are outside the normal rules of the game.
///
///E.G. Moving multiple piece, replacing a piece with another piece, other limitations
#[derive(PartialEq, Eq, Clone, Debug, Copy)]
pub enum SpecialMove {
    Castle,
    EnPassant,
    Promotion(ChessPiece),
}

///A Move is a potential move that can be made with the given board. must be validated before execution.
#[derive(Eq, PartialEq, Clone, Debug, Copy)]
pub struct Move {
    start_index: (usize, usize),
    end_index: (usize, usize),
    validated: bool,
    board: Board,
    special: Option<SpecialMove>,
}
impl Move {
    ///Constructs a new move.
    ///may fail if the given board has an ended game on it.
    pub fn new(
        board: Board,
        start_index: (usize, usize),
        end_index: (usize, usize),
        special: Option<SpecialMove>,
    ) -> Result<Self, ChessError> {
        if start_index.0 < 8 && start_index.1 < 8 && end_index.0 < 8 && end_index.1 < 8 {
            Ok(Move {
                board,
                start_index,
                end_index,
                validated: false,
                special,
            })
        } else {
            Err(ChessError::InvalidIndex)
        }
    }
    ///Return true if the move has been validated and is valid
    pub fn is_valid(&self) -> bool {
        self.validated
    }

    ///Gets the board this move is based on.
    pub fn get_board(&self) -> Board {
        self.board
    }
    ///Attempts to validate the move without any check checks.
    fn validate_without_check(&mut self) -> Result<(), ValidationFailure> {
        if let Some(sm) = self.special {
            match sm {
                SpecialMove::Castle => {
                    self.validate_castle()?;
                }
                SpecialMove::EnPassant => return Err(ValidationFailure::NotImplemented),
                SpecialMove::Promotion(_) => return Err(ValidationFailure::NotImplemented),
            }
        }
        match self.board.get_piece(self.start_index) {
            None => return Err(ValidationFailure::NoPieceAtIndex),
            Some(piece) => match &piece.piece {
                King => is_valid_king_move(self)?,
                Queen => is_valid_queen_move(self)?,
                Knight => is_valid_knight_move(self)?,
                Rook => is_valid_rook_move(self)?,
                Bishop => is_valid_bishop_move(self)?,
                Pawn => is_valid_pawn_move(self)?,
            },
        };

        self.validated = true;
        Ok(())
    }
    ///Code for checking if a castle is valid. Assumes the move is a castle
    fn validate_castle(&mut self) -> Result<(), ValidationFailure> {
        generic_move_validity_checks(&self, ChessPiece::King)?;
        let piece = self.board.get_piece(self.start_index).unwrap();
        if !self.board.get_player_info(piece.side).has_king_moved
            && !is_in_check(self.board, piece.side)
        {
            if !self.board.get_player_info(piece.side).has_king_rook_moved {
                match piece.side {
                    super::Side::White => {
                        if self.board.get_piece((1, 0)).is_none()
                            && self.board.get_piece((2, 0)).is_none()
                            && !is_in_check(self.board, piece.side)
                        {
                            let mut m = Move::new(
                                self.board,
                                self.start_index,
                                (1, 0),
                                Some(SpecialMove::Castle),
                            )
                            .unwrap();
                            if !is_in_check(m.execute_without_validation().unwrap(), piece.side) {
                                self.validated = true;
                                return Ok(());
                            }
                        }
                    }
                    super::Side::Black => {
                        if self.board.get_piece((1, 7)).is_none()
                            && self.board.get_piece((2, 7)).is_none()
                        {
                            let mut m = Move::new(
                                self.board,
                                self.start_index,
                                (1, 0),
                                Some(SpecialMove::Castle),
                            )
                            .unwrap();
                            if !is_in_check(m.execute_without_validation().unwrap(), piece.side) {}
                        }
                    }
                }
            }
            if !self.board.get_player_info(piece.side).has_queen_rook_moved {
                match piece.side {
                    super::Side::White => {
                        if self.board.get_piece((5, 0)).is_none()
                            && self.board.get_piece((4, 0)).is_none()
                            && self.board.get_piece((6, 0)).is_none()
                        {
                            let mut m = Move::new(
                                self.board,
                                self.start_index,
                                (5, 0),
                                Some(SpecialMove::Castle),
                            )
                            .unwrap();
                            if !is_in_check(m.execute_without_validation().unwrap(), piece.side) {}
                        }
                    }
                    super::Side::Black => {
                        if self.board.get_piece((5, 7)).is_none()
                            && self.board.get_piece((4, 7)).is_none()
                            && self.board.get_piece((6, 7)).is_none()
                        {
                            let mut m = Move::new(
                                self.board,
                                self.start_index,
                                (5, 7),
                                Some(SpecialMove::Castle),
                            )
                            .unwrap();
                            if !is_in_check(m.execute_without_validation().unwrap(), piece.side) {
                                self.validated = true;
                                return Ok(());
                            }
                        }
                    }
                }
            }
        }
        return Err(ValidationFailure::InvalidCastle);
    }

    ///Forcibly sets the move to valid without any checks
    pub unsafe fn force_validate(&mut self) {
        self.validated = true;
    }

    ///Checks if the move is a valid move to make.
    pub fn validate(&mut self) -> Result<(), ValidationFailure> {
        if self.validated {
            return Ok(());
        }
        self.validate_without_check()?;
        let sample = self.execute_without_validation().unwrap();
        let piece = self.board.get_piece(self.start_index).unwrap();
        if is_in_check(sample, piece.side) {
            self.validated = false;
            return Err(ValidationFailure::MoveIntoCheck);
        }
        Ok(())
    }
    ///Attempts to execute the move even if it isn't valid.
    ///This can be used for checking if the move moves the player into check.
    pub fn execute_without_validation(&self) -> Result<Board, ValidationFailure> {
        let mut board = self.board;
        if let Some(piece) = *board.get_piece(self.start_index) {
            if piece.piece == ChessPiece::King {
                board.set_player_info(piece.side, Some(true), None, None);
            }
            if piece.piece == ChessPiece::Rook {
                match piece.side {
                    White => {
                        if self.start_index == (0, 0) {
                            board.set_player_info(piece.side, None, Some(true), None);
                        } else if self.start_index == (7, 0) {
                            board.set_player_info(piece.side, None, None, Some(true));
                        }
                    }
                    Black => {
                        if self.start_index == (0, 7) {
                            board.set_player_info(piece.side, None, Some(true), None);
                        } else if self.start_index == (7, 7) {
                            board.set_player_info(piece.side, None, None, Some(true));
                        }
                    }
                }
            }
        }
        if let Some(special) = self.special {
            match special {
                SpecialMove::Castle => {
                    if self.end_index.0 == 1 {
                        board.set_piece(self.end_index, *board.get_piece(self.start_index));
                        board.set_piece(self.start_index, None);
                        board.set_piece((2, 0), *board.get_piece((0, self.end_index.1)));
                        board.set_piece((0, self.end_index.1), None);
                    } else if self.end_index.0 == 5 {
                        board.set_piece(self.end_index, *board.get_piece(self.start_index));
                        board.set_piece(self.start_index, None);
                        board.set_piece(
                            (4, self.end_index.1),
                            *board.get_piece((7, self.end_index.1)),
                        );
                        board.set_piece((7, self.end_index.1), None);
                    }
                }
                SpecialMove::EnPassant => todo!(),
                SpecialMove::Promotion(_) => todo!(),
            }
            return Ok(board);
        }

        if let Some(piece) = self.board.get_piece(self.start_index) {
            board.set_piece(self.start_index, None);
            board.set_piece(self.end_index, Some(*piece));

            Ok(board)
        } else {
            Err(ValidationFailure::NoPieceAtIndex)
        }
    }
    ///Executes a valid move and returns the resulting board.
    pub fn execute(&self) -> Result<Board, ValidationFailure> {
        if self.is_valid() {
            //unwrapping is ok becausue a piece must exist to be validated
            let piece = self.board.get_piece(self.start_index).unwrap();
            let mut board = self.execute_without_validation()?;
            let no_moves = all_valid_moves(board, piece.side.invert()).unwrap().len() == 0;
            let check = is_in_check(board, piece.side.invert());
            if no_moves {
                match check {
                    true => board.set_end_state(Some(EndGameState::Checkmate), piece.side.invert()),
                    false => {
                        board.set_end_state(Some(EndGameState::Stalemate), piece.side.invert())
                    }
                }
            }
            Ok(board)
        } else {
            Err(ValidationFailure::ExecuteInvalidMove)
        }
    }

    pub fn get_piece(&self) -> Option<GamePiece> {
        *self.board.get_piece(self.start_index)
    }
    ///gets the starting index of the move.
    pub fn get_start_index(&self) -> (usize, usize) {
        self.start_index
    }
    ///gets the ending index of the move.
    pub fn get_end_index(&self) -> (usize, usize) {
        self.end_index
    }
}
///Sees if the given player is in check on the given board.
pub fn is_in_check(board: Board, side: Side) -> bool {
    let kp = GamePiece::new(King, side);
    let mut king = (0, 0);
    'kingfind: for y in 0..8 {
        for x in 0..8 {
            if let Some(piece) = board.get_piece((x, y)) {
                if *piece == kp {
                    king = (x, y);
                    break 'kingfind;
                }
            }
        }
    }

    for y in 0..8 {
        for x in 0..8 {
            if let Some(piece) = board.get_piece((x, y)) {
                if piece.side != side
                    && get_valid_moves_without_check(board, (x, y))
                        .unwrap()
                        .into_iter()
                        .any(|end| end.get_end_index() == king)
                {
                    return true;
                }
            }
        }
    }

    false
}
///Gets all valid moves from a given start position without check checks.
fn get_valid_moves_without_check(
    board: Board,
    start_index: (usize, usize),
) -> Result<Vec<Move>, ChessError> {
    let mut list = Vec::new();
    for x in 0..8 {
        for y in 0..8 {
            let mut tested_move = Move::new(board, start_index, (x, y), None)?;
            let _ = tested_move.validate_without_check();
            if tested_move.is_valid() {
                list.push(tested_move);
            }
        }
    }
    Ok(list)
}
///Gets all valid moves from a given start position.
pub fn all_valid_moves(board: Board, side: Side) -> Result<Vec<Move>, ChessError> {
    let mut moves: Vec<Move> = Vec::new();
    for y in 0..8 {
        for x in 0..8 {
            if let Some(piece) = board.get_piece((x, y)) {
                if piece.side == side {
                    moves.extend(chess_move_generation::get_valid_moves(board, (x, y)));
                }
            }
        }
    }
    Ok(moves)
}

fn is_valid_king_move(game_move: &Move) -> Result<(), ValidationFailure> {
    generic_move_validity_checks(game_move, King)?;

    //unwrapping is ok because generic_validity_checks validates that there is a piece
    let piece = game_move
        .board
        .get_piece(game_move.start_index)
        .as_ref()
        .unwrap();

    let x_delta: i32 = game_move.end_index.0 as i32 - game_move.start_index.0 as i32;
    let y_delta: i32 = game_move.end_index.1 as i32 - game_move.start_index.1 as i32;
    //Is the move within one space of the start location?
    if x_delta.abs() > 1 || y_delta.abs() > 1 {
        return Err(ValidationFailure::InvalidMotion { piece: King });
    }

    match game_move.board.get_piece(game_move.end_index) {
        //There is no piece as the destination
        None => Ok(()),
        Some(captured_piece) => {
            //The piece at the destination is the same side as the current piece so the move is not valid
            if captured_piece.side == piece.side {
                Err(ValidationFailure::CaptureFriendlyPiece)
            //This move would capture an enemy piece and is valid
            } else {
                Ok(())
            }
        }
    }
}

fn is_valid_queen_move(game_move: &Move) -> Result<(), ValidationFailure> {
    generic_move_validity_checks(game_move, Queen)?;
    //unwrapping is ok because generic_validity_checks validates that there is a piece
    let piece = game_move
        .board
        .get_piece(game_move.start_index)
        .as_ref()
        .unwrap();
    let x_delta: i32 = game_move.end_index.0 as i32 - game_move.start_index.0 as i32;
    let y_delta: i32 = game_move.end_index.1 as i32 - game_move.start_index.1 as i32;

    //does the piece move in a straight line
    if !(x_delta == 0 || y_delta == 0 || x_delta.abs() == y_delta.abs()) {
        return Err(ValidationFailure::InvalidMotion { piece: Queen });
    }

    //are there any pieces in the way?
    if check_obstructions(game_move) {
        return Err(ValidationFailure::Obstruction);
    }
    //if there is a piece at the end location, is it on the enemy side?
    if let Some(captured_piece) = game_move.board.get_piece(game_move.end_index) {
        if captured_piece.side == piece.side {
            return Err(ValidationFailure::CaptureFriendlyPiece);
        }
    }

    Ok(())
}

fn is_valid_knight_move(game_move: &Move) -> Result<(), ValidationFailure> {
    generic_move_validity_checks(game_move, Knight)?;
    //unwrapping is ok because generic_validity_checks validates that there is a piece
    let piece = game_move
        .board
        .get_piece(game_move.start_index)
        .as_ref()
        .unwrap();
    let x_delta: i32 = game_move.end_index.0 as i32 - game_move.start_index.0 as i32;
    let y_delta: i32 = game_move.end_index.1 as i32 - game_move.start_index.1 as i32;
    //Does the piece move in an L shape?
    if (x_delta.abs() == 1 && y_delta.abs() == 2) || (x_delta.abs() == 2 && y_delta.abs() == 1) {
        //is the destination valid? eg is there a Friendly piece at the destination
        match game_move.board.get_piece(game_move.end_index) {
            None => Ok(()),
            Some(captured_piece) => {
                if captured_piece.side == piece.side {
                    Err(ValidationFailure::CaptureFriendlyPiece)
                } else {
                    Ok(())
                }
            }
        }
    } else {
        Err(ValidationFailure::InvalidMotion { piece: Knight })
    }
}

fn is_valid_rook_move(game_move: &Move) -> Result<(), ValidationFailure> {
    generic_move_validity_checks(game_move, Rook)?;
    //unwrapping is ok because generic_validity_checks validates that there is a piece
    let piece = game_move
        .board
        .get_piece(game_move.start_index)
        .as_ref()
        .unwrap();
    let x_delta: i32 = game_move.end_index.0 as i32 - game_move.start_index.0 as i32;
    let y_delta: i32 = game_move.end_index.1 as i32 - game_move.start_index.1 as i32;

    //does the piece move vertically or horizontally
    if !(x_delta == 0 || y_delta == 0) {
        return Err(ValidationFailure::InvalidMotion { piece: Rook });
    }

    //are there any pieces in the way?
    if check_obstructions(game_move) {
        return Err(ValidationFailure::Obstruction);
    }
    //if there is a piece at the end location, is it on the enemy side?
    if let Some(captured_piece) = game_move.board.get_piece(game_move.end_index) {
        if captured_piece.side == piece.side {
            return Err(ValidationFailure::CaptureFriendlyPiece);
        }
    }

    Ok(())
}

fn is_valid_bishop_move(game_move: &Move) -> Result<(), ValidationFailure> {
    generic_move_validity_checks(game_move, Bishop)?;
    //unwrapping is ok because generic_validity_checks validates that there is a piece
    let piece = game_move
        .board
        .get_piece(game_move.start_index)
        .as_ref()
        .unwrap();
    let x_delta: i32 = game_move.end_index.0 as i32 - game_move.start_index.0 as i32;
    let y_delta: i32 = game_move.end_index.1 as i32 - game_move.start_index.1 as i32;

    //does the piece move diagonally?
    if x_delta.abs() != y_delta.abs() {
        return Err(ValidationFailure::InvalidMotion { piece: Bishop });
    }

    //are there any pieces in the way?
    if check_obstructions(game_move) {
        return Err(ValidationFailure::Obstruction);
    }
    //if there is a piece at the end location, is it on the enemy side?
    if let Some(captured_piece) = game_move.board.get_piece(game_move.end_index) {
        if captured_piece.side == piece.side {
            return Err(ValidationFailure::CaptureFriendlyPiece);
        }
    }

    Ok(())
}

fn is_valid_pawn_move(game_move: &Move) -> Result<(), ValidationFailure> {
    generic_move_validity_checks(game_move, Pawn)?;

    if game_move.special.is_some() {
        return Err(ValidationFailure::NotImplemented);
    }
    let piece = game_move
        .board
        .get_piece(game_move.start_index)
        .as_ref()
        .unwrap();

    let x_delta: i32 = game_move.end_index.0 as i32 - game_move.start_index.0 as i32;
    let y_delta: i32 = game_move.end_index.1 as i32 - game_move.start_index.1 as i32;
    let has_moved = match piece.side {
        White => game_move.start_index.1 != 1,
        Black => game_move.start_index.1 != 6,
    };

    //is the Pawn moving in the correct direction
    match piece.side {
        White => {
            if y_delta <= 0 {
                return Err(ValidationFailure::InvalidMotion { piece: Pawn });
            }
        }
        Black => {
            if y_delta >= 0 {
                return Err(ValidationFailure::InvalidMotion { piece: Pawn });
            }
        }
    }
    //normal movement
    if x_delta == 0 {
        if y_delta.abs() == 1 && game_move.board.get_piece(game_move.end_index).is_none() {
            return Ok(());
        }
        if y_delta.abs() == 2
            && !has_moved
            && game_move.board.get_piece(game_move.end_index).is_none()
            && game_move
                .board
                .get_piece((
                    game_move.start_index.0,
                    (game_move.start_index.1 + game_move.end_index.1) / 2,
                ))
                .is_none()
        {
            return Ok(());
        }
    }

    //pawn capture
    if x_delta.abs() == 1 && y_delta.abs() == 1 {
        if let Some(captured_piece) = game_move.board.get_piece(game_move.end_index) {
            if captured_piece.side != piece.side {
                return Ok(());
            }
        }
    }

    //TODO Implement En passant

    //no valid moves found
    Err(ValidationFailure::InvalidMotion { piece: Pawn })
}

fn generic_move_validity_checks(
    game_move: &Move,
    piece_type: ChessPiece,
) -> Result<(), ValidationFailure> {
    //Is there a piece at the start location?
    if let Some(piece) = game_move.board.get_piece(game_move.start_index) {
        //Is the piece at the start location the right type?
        if piece_type == piece.piece {
            //Does the piece acually move?
            if game_move.start_index != game_move.end_index {
                //index check
                Ok(())
            }
            //The piece does not move so this move is not valid
            else {
                Err(ValidationFailure::MustMove)
            }
        }
        //The piece at the start location is not the right type so this is not a valid move.
        else {
            Err(ValidationFailure::IncorrectPiece)
        }
    }
    //There is no piece at the start location so this move is not valid.
    else {
        Err(ValidationFailure::NoPieceAtIndex)
    }
}
fn check_obstructions(game_move: &Move) -> bool {
    let x_delta: i32 = game_move.end_index.0 as i32 - game_move.start_index.0 as i32;
    let y_delta: i32 = game_move.end_index.1 as i32 - game_move.start_index.1 as i32;
    match Direction::new(x_delta, y_delta) {
        Up => {
            let mut ob = false;
            for i in game_move.end_index.1 + 1..game_move.start_index.1 {
                if game_move
                    .board
                    .get_piece((game_move.start_index.0, i))
                    .is_some()
                {
                    ob = true;
                    break;
                }
            }
            ob
        }
        Down => {
            let mut ob = false;
            for i in game_move.start_index.1 + 1..game_move.end_index.1 {
                if game_move
                    .board
                    .get_piece((game_move.start_index.0, i))
                    .is_some()
                {
                    ob = true;
                    break;
                }
            }
            ob
        }
        Left => {
            let mut ob = false;
            for i in game_move.end_index.0 + 1..game_move.start_index.0 {
                if game_move
                    .board
                    .get_piece((i, game_move.start_index.1))
                    .is_some()
                {
                    ob = true;
                    break;
                }
            }
            ob
        }
        Right => {
            let mut ob = false;
            for i in game_move.start_index.0 + 1..game_move.end_index.0 {
                if game_move
                    .board
                    .get_piece((i, game_move.start_index.1))
                    .is_some()
                {
                    ob = true;
                    break;
                }
            }
            ob
        }
        UpLeft => {
            let mut ob = false;
            for i in 1..x_delta.abs() {
                if game_move
                    .board
                    .get_piece((
                        game_move.start_index.0 - i as usize,
                        game_move.start_index.1 - i as usize,
                    ))
                    .is_some()
                {
                    ob = true;
                    break;
                }
            }
            ob
        }
        UpRight => {
            let mut ob = false;
            for i in 1..x_delta.abs() {
                if game_move
                    .board
                    .get_piece((
                        game_move.start_index.0 + i as usize,
                        game_move.start_index.1 - i as usize,
                    ))
                    .is_some()
                {
                    ob = true;
                    break;
                }
            }
            ob
        }
        DownLeft => {
            let mut ob = false;
            for i in 1..x_delta.abs() {
                if game_move
                    .board
                    .get_piece((
                        game_move.start_index.0 - i as usize,
                        game_move.start_index.1 + i as usize,
                    ))
                    .is_some()
                {
                    ob = true;
                    break;
                }
            }
            ob
        }
        DownRight => {
            let mut ob = false;
            for i in 1..x_delta.abs() {
                if game_move
                    .board
                    .get_piece((
                        game_move.start_index.0 + i as usize,
                        game_move.start_index.1 + i as usize,
                    ))
                    .is_some()
                {
                    ob = true;
                    break;
                }
            }
            ob
        }
        NoDirection => {
            dbg!(x_delta, y_delta);
            false
        }
    }
}
