use std::fmt;
use thiserror::Error;
use ChessPiece::*;
use Side::*;
mod board;
pub use crate::chess::board::*;
mod chess_move;
pub use crate::chess::chess_move::*;

pub(crate) mod chess_move_generation;

pub(crate) mod chess_move_validation;

#[derive(Error, Debug)]
pub enum ChessError {
    #[error("The index given was invalid.")]
    InvalidIndex,
    #[error("The given move is invalid because {failure_reason}.")]
    ValidationFailed {
        #[from]
        failure_reason: ValidationFailure,
    },
    #[error("The game is over.")]
    GameIsOver,
    #[error("Error creating move: {creation_error}")]
    MoveCreation {
        #[from]
        creation_error: MoveCreationError,
    },
}

#[derive(Debug, Clone, Copy, Error)]
pub enum ValidationFailure {
    #[error("This move is not yet implemented")]
    NotImplemented,
    #[error("There must be a piece at the chosen index")]
    NoPieceAtIndex,
    #[error("The attempted castle is not valid")]
    InvalidCastle,
    #[error("You cannot move into check")]
    MoveIntoCheck,
    #[error("A move must be validated to be executed")]
    ExecuteInvalidMove,
    #[error("{piece:?} moved in a way that is not valid")]
    InvalidMotion { piece: ChessPiece },
    #[error("Cannot capture a friendly piece")]
    CaptureFriendlyPiece,
    #[error("There is a piece in the way")]
    Obstruction,
    #[error("There is a piece mismatch")]
    IncorrectPiece,
    #[error("The piece must move")]
    MustMove,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub struct Index {
    pub x: usize,
    pub y: usize,
}

impl Index {
    pub fn new(x: usize, y: usize) -> Self {
        Index { x, y }
    }

    pub fn translate(&self, deltas: (i32, i32)) -> Result<Self, ChessError> {
        //validate that the translation is possible
        if self.x as i32 + deltas.0 < 0
            || self.x as i32 + deltas.0 > 7
            || self.y as i32 + deltas.1 < 0
            || self.y as i32 + deltas.1 > 7
        {
            Err(ChessError::InvalidIndex)
        } else {
            Ok(Index::new(
                (self.x as i32 + deltas.0) as usize,
                (self.y as i32 + deltas.1) as usize,
            ))
        }
    }
}

#[derive(Eq, PartialEq, Copy, Clone, Debug)]
pub enum Side {
    White,
    Black,
}

impl Side {
    pub const fn invert(&self) -> Self {
        match self {
            White => Black,
            Black => White,
        }
    }
}

#[derive(Eq, PartialEq, Copy, Clone, Debug)]
pub enum ChessPiece {
    King,
    Queen,
    Knight,
    Rook,
    Bishop,
    Pawn,
}

#[derive(Eq, PartialEq, Copy, Clone, Debug)]
pub struct GamePiece {
    pub piece: ChessPiece,
    pub side: Side,
}

impl GamePiece {
    pub const fn new(piece: ChessPiece, side: Side) -> Self {
        GamePiece { piece, side }
    }
}

pub fn index_to_chess_location(index: (usize, usize)) -> Result<String, ChessError> {
    let mut string = String::new();
    string.push(match index.0 {
        0 => 'A',
        1 => 'B',
        2 => 'C',
        3 => 'D',
        4 => 'E',
        5 => 'F',
        6 => 'G',
        7 => 'H',
        _ => return Err(ChessError::InvalidIndex),
    });
    if index.1 > 7 {
        return Err(ChessError::InvalidIndex);
    }
    string.push_str(&(index.1 + 1).to_string());

    Ok(string)
}

pub fn chess_location_to_index(string: &str) -> Result<(usize, usize), ChessError> {
    if string.len() != 2 {
        return Err(ChessError::InvalidIndex);
    }
    let mut index: (usize, usize) = Default::default();
    let mut chars = string.chars();
    index.0 = match chars.next().ok_or(ChessError::InvalidIndex)? {
        'A' => 0,
        'B' => 1,
        'C' => 2,
        'D' => 3,
        'E' => 4,
        'F' => 5,
        'G' => 6,
        'H' => 7,
        _ => return Err(ChessError::InvalidIndex),
    };
    let vert = chars
        .next()
        .ok_or(ChessError::InvalidIndex)?
        .to_digit(10)
        .ok_or(ChessError::InvalidIndex)?;
    if vert <= 8 && vert > 0 {
        index.1 = (vert - 1) as usize;
    } else {
        return Err(ChessError::InvalidIndex);
    }
    Ok(index)
}

#[cfg(test)]
mod tests {
    use std::usize;

    use super::*;

    #[test]
    fn pawn_moves() {
        let mut pieces: Vec<(GamePiece, usize, usize)> = Vec::new();
        pieces.push((GamePiece::new(ChessPiece::Pawn, Side::White), 0, 1));
        pieces.push((GamePiece::new(ChessPiece::Pawn, Side::White), 1, 2));
        pieces.push((GamePiece::new(ChessPiece::Pawn, Side::Black), 0, 6));
        pieces.push((GamePiece::new(ChessPiece::Pawn, Side::White), 1, 5));

        let board = Board::new_custom(pieces);

        //Move white pawn forward one space, should validate
        let mut test_move = Move::new(board, (0, 1), (0, 2), None).unwrap();
        assert!(test_move.validate().is_ok());
        //Move white pawn forward two spaces, should validate
        let mut test_move = Move::new(board, (0, 1), (0, 3), None).unwrap();
        assert!(test_move.validate().is_ok());
        //Move white pawn forward three spaces, should not validate
        let mut test_move = Move::new(board, (0, 1), (0, 4), None).unwrap();
        assert!(test_move.validate().is_err());
        //Move white pawn forward three spaces off of starting row, should not validate
        let mut test_move = Move::new(board, (1, 2), (1, 4), None).unwrap();
        assert!(test_move.validate().is_err());
        //Move black pawn forward one space, should validate
        let mut test_move = Move::new(board, (0, 6), (0, 5), None).unwrap();
        assert!(test_move.validate().is_ok());
        //Move black pawn forward two spaces, should validate
        let mut test_move = Move::new(board, (0, 6), (0, 4), None).unwrap();
        assert!(test_move.validate().is_ok());
        //Move black pawn forward three spaces, should no validate
        let mut test_move = Move::new(board, (0, 6), (0, 3), None).unwrap();
        assert!(test_move.validate().is_err());
        //Move black pawn forward two spaces off of the starting row, should not validate
        let mut test_move = Move::new(board, (1, 5), (1, 3), None).unwrap();
        assert!(test_move.validate().is_err());
    }

    #[test]
    fn bishop_moves() {
        let mut pieces: Vec<(GamePiece, usize, usize)> = Vec::new();
        pieces.push((GamePiece::new(ChessPiece::Bishop, Side::White), 0, 0));
        pieces.push((GamePiece::new(ChessPiece::Bishop, Side::White), 5, 5));
        let board = Board::new_custom(pieces);

        let mut test_move = Move::new(board, (0, 0), (3, 3), None).unwrap();
        assert!(test_move.validate().is_ok());

        let mut test_move = Move::new(board, (0, 0), (7, 7), None).unwrap();
        assert!(test_move.validate().is_err());
    }
}
