use std::error::Error;
use std::fmt::{self, Display};

use anyhow::Context;
use anyhow::Result;

use sdl2::image::LoadTexture;
use sdl2::pixels::Color;
use sdl2::rect::Rect;
use sdl2::render::Canvas;
use sdl2::render::Texture;
use sdl2::render::TextureCreator;
use sdl2::video::Window;
use sdl2::video::WindowContext;
use sdl2::VideoSubsystem;
use sdl2::{EventPump, Sdl};

use crate::chess::{Board, Move};

pub struct RenderingContext {
    sdl_context: Sdl,
    _video_subsystem: VideoSubsystem,
    canvas: Canvas<Window>,
    pub square_size: u32,
    pub selected_square: Option<(usize, usize)>,
    pub potential_moves: Option<Vec<Move>>,
}

pub struct Textures<'a> {
    white_pawn: Texture<'a>,
    black_pawn: Texture<'a>,
    white_rook: Texture<'a>,
    black_rook: Texture<'a>,
    white_bishop: Texture<'a>,
    black_bishop: Texture<'a>,
    white_knight: Texture<'a>,
    black_knight: Texture<'a>,
    white_queen: Texture<'a>,
    black_queen: Texture<'a>,
    white_king: Texture<'a>,
    black_king: Texture<'a>,
}
#[derive(Debug)]
struct SdlError {
    error: String,
}

impl SdlError {
    pub fn new(error: String) -> Self {
        SdlError { error }
    }
}

impl Display for SdlError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.error)
    }
}

impl Error for SdlError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        None
    }
}

impl RenderingContext {
    pub fn new(width: u32, height: u32, title: &str) -> Result<Self> {
        let sdl_context = sdl2::init().map_err(SdlError::new)?;
        let video_subsystem = sdl_context.video().unwrap();
        let mut window = video_subsystem
            .window(title, width, height)
            .position_centered()
            .build()
            .context("Failed to Create Window")?;
        window
            .set_fullscreen(sdl2::video::FullscreenType::True)
            .map_err(SdlError::new)?;
        let mut canvas = window
            .into_canvas()
            .build()
            .context("Failed to create canvas")?;
        canvas.set_draw_color(Color::RGB(0, 255, 255));
        canvas.clear();

        let square_size = {
            if width < height {
                width / 8
            } else {
                height / 8
            }
        };

        Ok(RenderingContext {
            sdl_context,
            _video_subsystem: video_subsystem,
            canvas,
            square_size,
            selected_square: None,
            potential_moves: None,
        })
    }

    pub fn get_texture_creator(&self) -> TextureCreator<WindowContext> {
        self.canvas.texture_creator()
    }

    pub fn get_event_pump(&self) -> EventPump {
        self.sdl_context.event_pump().unwrap()
    }

    pub fn draw_board(&mut self, board: &Board, tex: &Textures) {
        self.canvas.set_draw_color(Color::MAGENTA);
        self.canvas.clear();
        let mut color = Color::BLACK;
        for y in 0..8 {
            //offsets color each row
            if color == Color::BLACK {
                color = Color::WHITE;
            } else {
                color = Color::BLACK;
            }
            for x in 0..8 {
                //alternates color between columns
                self.canvas.set_draw_color(color);
                if color == Color::BLACK {
                    color = Color::WHITE;
                } else {
                    color = Color::BLACK;
                }

                //highight selected square red
                if let Some((sx, sy)) = self.selected_square {
                    if x == sx && y == sy {
                        self.canvas.set_draw_color(Color::RED);
                    }
                }

                //highlight potential moves green
                if let Some(sq) = &self.potential_moves {
                    if sq
                        .iter()
                        .any(|m| x == m.get_end_index().0 && y == m.get_end_index().1)
                    {
                        self.canvas.set_draw_color(Color::GREEN);
                    }
                }

                //draw spaces
                self.canvas
                    .fill_rect(Rect::new(
                        x as i32 * self.square_size as i32,
                        y as i32 * self.square_size as i32,
                        self.square_size,
                        self.square_size,
                    ))
                    .unwrap();

                {
                    //draw pieces
                    let gp = board.get_piece((x as usize, y as usize));
                    if let Some(gp) = gp {
                        self.canvas
                            .copy(
                                match gp.side {
                                    crate::chess::Side::White => match gp.piece {
                                        crate::chess::ChessPiece::King => &tex.white_king,
                                        crate::chess::ChessPiece::Queen => &tex.white_queen,
                                        crate::chess::ChessPiece::Knight => &tex.white_knight,
                                        crate::chess::ChessPiece::Rook => &tex.white_rook,
                                        crate::chess::ChessPiece::Bishop => &tex.white_bishop,
                                        crate::chess::ChessPiece::Pawn => &tex.white_pawn,
                                    },
                                    crate::chess::Side::Black => match gp.piece {
                                        crate::chess::ChessPiece::King => &tex.black_king,
                                        crate::chess::ChessPiece::Queen => &tex.black_queen,
                                        crate::chess::ChessPiece::Knight => &tex.black_knight,
                                        crate::chess::ChessPiece::Rook => &tex.black_rook,
                                        crate::chess::ChessPiece::Bishop => &tex.black_bishop,
                                        crate::chess::ChessPiece::Pawn => &tex.black_pawn,
                                    },
                                },
                                None,
                                Some(Rect::new(
                                    x as i32 * self.square_size as i32,
                                    y as i32 * self.square_size as i32,
                                    self.square_size,
                                    self.square_size,
                                )),
                            )
                            .unwrap();
                    }
                }
            }
        }
        self.canvas.present();
    }
}

impl Textures<'_> {
    pub fn new(creator: &TextureCreator<WindowContext>) -> Textures {
        let white_pawn = creator
            .load_texture("res/w_pawn_png_shadow_512px.png")
            .unwrap();
        let black_pawn = creator
            .load_texture("res/b_pawn_png_shadow_512px.png")
            .unwrap();
        let white_rook = creator
            .load_texture("res/w_rook_png_shadow_512px.png")
            .unwrap();
        let black_rook = creator
            .load_texture("res/b_rook_png_shadow_512px.png")
            .unwrap();
        let white_bishop = creator
            .load_texture("res/w_bishop_png_shadow_512px.png")
            .unwrap();
        let black_bishop = creator
            .load_texture("res/b_bishop_png_shadow_512px.png")
            .unwrap();
        let white_knight = creator
            .load_texture("res/w_knight_png_shadow_512px.png")
            .unwrap();
        let black_knight = creator
            .load_texture("res/b_knight_png_shadow_512px.png")
            .unwrap();
        let white_queen = creator
            .load_texture("res/w_queen_png_shadow_512px.png")
            .unwrap();
        let black_queen = creator
            .load_texture("res/b_queen_png_shadow_512px.png")
            .unwrap();
        let white_king = creator
            .load_texture("res/w_king_png_shadow_512px.png")
            .unwrap();
        let black_king = creator
            .load_texture("res/b_king_png_shadow_512px.png")
            .unwrap();
        Textures {
            white_pawn,
            black_pawn,
            white_rook,
            black_rook,
            white_bishop,
            black_bishop,
            white_knight,
            black_knight,
            white_queen,
            black_queen,
            white_king,
            black_king,
        }
    }
}
